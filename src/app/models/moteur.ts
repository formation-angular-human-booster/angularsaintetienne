export class Moteur {

  model: string;
  nbCheveaux: number;
  constructor(model: string = null, nbCheveaux: number = null) {
    this.model = model;
    this.nbCheveaux = nbCheveaux;
  }
}
