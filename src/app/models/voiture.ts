import {Moteur} from './moteur';

export class Voiture {
  nbPortes: number;
  couleur: string;
  carburant: string;
  boiteAutomatique: boolean;
  freinAMainEnclenche: boolean;
  moteur: Moteur;
  constructor(nbPortes: number = null, couleur: string = null, carburant: string = null, boiteAutomatique: boolean = null, freinAMainEnclenche = false, moteur = null) {
    this.nbPortes = nbPortes;
    this.couleur = couleur;
    this.carburant = carburant;
    this.boiteAutomatique = boiteAutomatique;
    this.freinAMainEnclenche = freinAMainEnclenche;
    this.moteur = moteur;
  }
  rouler(): void {
    this.freinAMainEnclenche = false;
  }
  getCarburant(): string {
    return this.carburant;
  }
}

