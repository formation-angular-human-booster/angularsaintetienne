export class Article {
  title: string;
  contenu: string;
  image: string;
  constructor(title: string, contenu: string, image: string) {
    this.title = title;
    this.contenu = contenu;
    this.image = image;
  }
}
