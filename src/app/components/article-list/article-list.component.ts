import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {
  composant = 'Article List composant';
  variable = 'Ma variable du composant parent';
  article1 = new Article('Super match au michelin', 'L\'ASM a gagné', '1.jpg');
  article2 = new Article('La fin du corona', 'On va au bar', '2.jpg');
  article3 = new Article('Black live matter', 'Sympa l\'actu', '3.jpg');
  article4 = new Article('TEST', 'TEST', '3.jpg');

  tableauArticle = [this.article1, this.article2, this.article3, this.article4];

  constructor() { }
  ngOnInit(): void {
  }

}
