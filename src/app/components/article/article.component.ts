import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../models/article';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input() article: Article;

  composant = 'Article Component';
  resultatAddition: number;

  constructor() {
    this.resultatAddition = this.additionner(1, 2);

  }

  ngOnInit(): void {

  }

  additionner(chiffre1: number, chiffre2: number): number {
    return chiffre1 + chiffre2;
  }

  clickEffectue(): void {
    console.log('Tu as cliqué dans un bouton du composant' + this.composant);
  }

  dblClickEffectue(): void {
    alert('Double click effectue');
  }
}
