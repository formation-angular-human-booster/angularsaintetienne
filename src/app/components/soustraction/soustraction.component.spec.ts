import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoustractionComponent } from './soustraction.component';

describe('SoustractionComponent', () => {
  let component: SoustractionComponent;
  let fixture: ComponentFixture<SoustractionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoustractionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoustractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
