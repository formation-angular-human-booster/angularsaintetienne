import { Component, OnInit } from '@angular/core';
import {Voiture} from '../../models/voiture';
import {Moteur} from '../../models/moteur';

@Component({
  selector: 'app-garage',
  templateUrl: './garage.component.html',
  styleUrls: ['./garage.component.css']
})
export class GarageComponent implements OnInit {
  peoples = ['Aurélien', 'Pierre', 'Thomas'];
  cars: Voiture[];
  constructor() {
    this.cars = [
      new Voiture(5, 'Bleue', 'Diesel',
        true, false,
        new Moteur('Mercedes', 150)),
      new Voiture(2, 'Rouge', 'Esssence',
        false, false,
        new Moteur('BMW', 500))
    ];

    console.log(this.cars);
  }

  ngOnInit(): void {
  }

}
