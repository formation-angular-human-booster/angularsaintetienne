import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toogle-image',
  templateUrl: './toogle-image.component.html',
  styleUrls: ['./toogle-image.component.css']
})
export class ToogleImageComponent implements OnInit {
  dislpayImage = false;
  mood = 'Happy';
  displayText = false;


  constructor() { }

  ngOnInit(): void {
  }
  toogleImage(): void {
    this.dislpayImage = !this.dislpayImage;
    console.log(this.dislpayImage);
  }

  displayParagraphe() {
    this.displayText = true;
  }

  changerHumeur(humeur: string) {
    this.mood = humeur;
  }
}
