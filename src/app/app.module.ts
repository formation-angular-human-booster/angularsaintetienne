import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ArticleComponent } from './components/article/article.component';
import {ModalModule} from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import {MenuComponent} from './components/menu/menu.component';
import { ArticleListComponent } from './components/article-list/article-list.component';
import { AdditionComponent } from './components/addition/addition.component';
import { SoustractionComponent } from './components/soustraction/soustraction.component';
import { MultiplicationComponent } from './components/multiplication/multiplication.component';
import { CalculetteComponent } from './components/calculette/calculette.component';
import { ToogleImageComponent } from './components/toogle-image/toogle-image.component';
import { GarageComponent } from './components/garage/garage.component';

@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    MenuComponent,
    ArticleListComponent,
    AdditionComponent,
    SoustractionComponent,
    MultiplicationComponent,
    CalculetteComponent,
    ToogleImageComponent,
    GarageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    CarouselModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
