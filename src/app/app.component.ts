import { Component } from '@angular/core';
import {Voiture} from './models/voiture';
import {Moteur} from './models/moteur';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'firstProject';
  composant = 'AppComponent';
  voiture = new Voiture();

  constructor() {
    this.voiture = new Voiture();
    this.voiture.moteur = new Moteur();
    this.voiture.moteur.model = 'Mercedes';
    this.voiture.moteur.nbCheveaux = 500;
  }

}
